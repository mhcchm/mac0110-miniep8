function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if comparebyvalueandsuit(v[j], v[j-1])
                troca(v, j, j-1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

function comparebyvalue(x, y)
    valorx = 0
    valory = 0

    if x[1] == '1' && x[2] == '0'
        valorx = 10
    elseif x[1] == 'J'
        valorx = 11
    elseif x[1] == 'Q'
        valorx = 12
    elseif x[1] == 'K'
        valorx = 13
    elseif x[1] == 'A'
        valorx = 14
    elseif x[1] == '2'
        valorx = 2
    elseif x[1] == '3'
        valorx = 3
    elseif x[1] == '4'
        valorx = 4
    elseif x[1] == '5'
        valorx = 5
    elseif x[1] == '6'
        valorx = 6
    elseif x[1] == '7'
        valorx = 7
    elseif x[1] == '8'
        valorx = 8
    elseif x[1] == '9'
        valorx = 9
    else
        valorx = 10
    end

    if y[1] == '1' && y[2] == '0'
        valory = 10
    elseif y[1] == 'J'
        valory = 11
    elseif y[1] == 'Q'
        valory = 12
    elseif y[1] == 'K'
        valory = 13
    elseif y[1] == 'A'
        valory = 14
    elseif y[1] == '2'
        valory = 2
    elseif y[1] == '3'
        valory = 3
    elseif y[1] == '4'
        valory = 4
    elseif y[1] == '5'
        valory = 5
    elseif y[1] == '6'
        valory = 6
    elseif y[1] == '7'
        valory = 7
    elseif y[1] == '8'
        valory = 8
    elseif y[1] == '9'
        valory = 9
    else
        valory = 10
    end

    if valorx < valory
        return true
    else
        return false
    end
    
end

function comparebyvalueandsuit(x, y)
    px = 2
    py = 2

    if x[2] == '0'
        px = 3
    elseif y[2] == '0'
        py = 3
    end

    if x[px] == y[py]
        return comparebyvalue(x, y)
    else
        valornaipex = 0
        valornaipey = 0
        
        if x[px] == '♦'
            valornaipex = 1
        elseif x[px] == '♠'
            valornaipex = 2
        elseif x[px] == '♥'
            valornaipex = 3
        else
            valornaipex = 4
        end

        if y[py] == '♦'
            valornaipey = 1
        elseif y[py] == '♠'
            valornaipey = 2
        elseif y[py] == '♥'
            valornaipey = 3
        else
            valornaipey = 4
        end

        if valornaipex < valornaipey
            return true
        else
            return false
        end
    end
end

using Test

function testa()
    @test comparebyvalue("2♠", "A♠")
    @test !comparebyvalue("K♥", "10♥")
    @test !comparebyvalue("10♠", "10♥")
    @test comparebyvalueandsuit("2♠", "A♠")
    @test !comparebyvalueandsuit("K♥", "10♥")
    @test comparebyvalueandsuit("10♠", "10♥")
    @test comparebyvalueandsuit("A♠", "2♥")
    @test !comparebyvalueandsuit("A♠", "A♠")
    @test comparebyvalueandsuit("A♠", "10♥")
    @test comparebyvalueandsuit("9♦", "10♦")
    println("Tudo ok :)")
end

#testa()